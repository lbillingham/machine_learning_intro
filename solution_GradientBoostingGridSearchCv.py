gb = GradientBoostingRegressor(random_state=42)
parameters = {'n_estimators':[10, 20, 50],
              'max_depth':[5, 7, 9, 11, 13]}

clf_grid_gb = grid_search.GridSearchCV(gb, parameters, n_jobs=-1)
clf_grid_gb.fit(X_train, y_train)
gb_best = clf_grid_gb.best_estimator_
print(70*'=')
print ('best fit {0}: {1} estimators, max_depth={2}'.format(clf_grid_gb.estimator.__class__.__name__, 
                                                            clf_grid_gb.best_estimator_.n_estimators, 
                                                            clf_grid_gb.best_estimator_.max_depth))
print(70*'=')

y_fit_gb_best = gb_best.predict(X_test)
legnd_tail = ': n:{0}, depth:{1}'
plt.plot(X_test.ravel(), y_fit_gb_best, color='blue',
        label='best fit GradientBoostingRegressor' + legnd_tail.format(gb_best.n_estimators, gb_best.max_depth))
plt.plot(X_train.ravel(), y_train, '.k', label='noisy samples')
#plt.plot(X.ravel(), y_true, color='red', label='ground truth')
plt.xlabel('samples')
plt.ylabel('target')
plt.legend()


#-----------------------------------------------------------------------------------------------------------------
print ('')
print ('')
print (105*'-')
print ('')
print("""The depth of the best GradientBoostingRegressor should be < that of the best RandomForestRegressor.
		We can think of RandomForests as averaging over many over-fitting models and 
		GradientBoosting as gradually refining under-fitting models""")
print ('')
print (105*'-')


#-----------------------------------------------------------------------------------------------------------------

for cv_fitted in [clf_grid_gb, clf_grid_rf]:
	print('best-fit {0} gives score of {1}'.format(cv_fitted.best_estimator_.__class__.__name__, cv_fitted.best_score_))

